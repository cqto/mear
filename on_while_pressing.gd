extends Node

@export var target: Node2D
@export var action: StringName

func _ready():
	if not (target.has_method("turn_on") and target.has_method("turn_off")):
		assert(false, "Target must implement required methods") 


func _unhandled_input(event):
	if event.is_action_pressed(action):
		target.turn_on()
	elif event.is_action_released(action):
		target.turn_off()
