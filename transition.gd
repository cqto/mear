extends Node

const white_text_scene = preload("res://texto.tscn")
const black_text_scene = preload("res://TextoNegro.tscn")
const rocks_scene = preload("res://rocas.tscn")
const end_scene = preload("res://fin.tscn")

const TEXT_RES_PATH = "res://texto/%s.tres"

const scenes = [
	{"type": "white", "name": "tutorial"},
	{"type": "rock"},
	{"type": "white", "name": "1"},
	{"type": "rock"},
	{"type": "white", "name": "2"},
	{"type": "black", "name": "3"},
	{"type": "white", "name": "4"},
	{"type": "rock"},
	{"type": "white", "name": "5"},
	{"type": "black", "name": "6"},
	{"type": "rock"},
	{"type": "white", "name": "7"},
	{"type": "black", "name": "8"},
	{"type": "white", "name": "9"},
	{"type": "black", "name": "10"},
	{"type": "white", "name": "11"},
	{"type": "rock"},
	{"type": "white", "name": "12"},
	{"type": "black", "name": "13"},
	{"type": "white", "name": "14"},
	{"type": "fin"},
]
var _current_idx := 0


func _ready():
	TransitionActions.to_next_scene.connect(switch_to_next_scene)
	transition_to_next_scene()


func switch_to_next_scene():
	_current_idx += 1
	if _current_idx < len(scenes):
		transition_to_next_scene()


func transition_to_next_scene():
	var scene_data = scenes[_current_idx]
	if scene_data["type"] == "rock":
		transition_to_rocks()
	elif scene_data["type"] == "fin":
		transition_to_end()
	else:
		transition_to_text(scene_data["name"], scene_data["type"] == "black")
	

func transition_to_rocks():
	_swap_current_with(rocks_scene.instantiate())


func transition_to_end():
	_swap_current_with(end_scene.instantiate())


func transition_to_text(text_res_name: StringName, is_black = false):
	var text_res = load(TEXT_RES_PATH % text_res_name)
	var text_instance = (black_text_scene if is_black else white_text_scene).instantiate()
	text_instance.config = text_res
	_swap_current_with(text_instance)


func _swap_current_with(new_scene):
	remove_child(get_tree().get_first_node_in_group("CurrentScene"))
	add_child(new_scene)
	new_scene.add_to_group("CurrentScene")
