extends Node2D

@export var config: TextConfig

@onready var timer = $Timer
@onready var label = $Label

var _text_has_finished := false
var _next_text_idx := 0
var _text_list: Array

func _ready():
	_text_list = config.text
	timer.wait_time = 0.075 if config.text_speed == "fast" else 0.15
	label.has_started.connect(func(): _text_has_finished = false)
	label.has_finished.connect(func(): _text_has_finished = true)
	_to_next_text()


func _to_next_text():
	if _next_text_idx >= len(_text_list):
		TransitionActions.to_next_scene.emit()
	else:
		label.set_and_start(_text_list[_next_text_idx])
		_next_text_idx += 1



func _input(event):
	if event.is_action_pressed("seguir"):
		if _text_has_finished:
			_to_next_text()
		else:
			label.set_max_visible_characters()
			
	
