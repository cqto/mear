extends Node2D

# Milliseconds of being peed on before the lizard comes out
@export_range(1000, 3000) var pee_threshold := 1500

@onready var lagarto := $Lagarto
@onready var area := $Area2D

# Mouse on top
var _targeted := false
var _pee_ms := 0


func _ready():
	area.mouse_entered.connect(func(): _targeted = true)
	area.mouse_exited.connect(func(): _targeted = false)


func _process(delta):
	handle_lagarto(delta)


func handle_lagarto(delta):
	if lagarto.active:
		return

	if _pee_ms >= pee_threshold:
		lagarto.activate()
		return 

	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) and _targeted:
		add_pee(delta)

func add_pee(amount):
	_pee_ms += (amount * 1000)
