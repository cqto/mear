extends Node

@export var target: Node2D

func _process(_delta):
	target.position = get_viewport().get_mouse_position()