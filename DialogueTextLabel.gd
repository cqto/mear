extends Label


signal character_shown
signal has_started
signal has_finished


func set_and_start(new_text: String):
	visible_characters = 0
	text = new_text
	has_started.emit()


func increment_visible_characters_by_one():
	if(visible_characters < get_total_character_count()):
		visible_characters += 1
		character_shown.emit()
	else:
		has_finished.emit()



func set_max_visible_characters():
	visible_characters = get_total_character_count()
	has_finished.emit()


func is_totally_visible() -> bool:
	return visible_characters == get_total_character_count()
