extends Node2D

@onready var chorrito := $Chorro

func turn_on():
	chorrito.emitting = true


func turn_off():
	chorrito.emitting = false