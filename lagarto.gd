extends Node2D
class_name Lagarto

signal caught

@onready var pop_sfx := $PopSFX
@onready var collision := %Collision
@onready var area := %Area2D

var active := false
var _targeted := false


func _ready():
	hide()
	area.mouse_entered.connect(func(): _targeted = true)
	area.mouse_exited.connect(func(): _targeted = false)


func _process(_delta):
	if active:
		handle_active()


func activate():
	show()
	pop_sfx.play()
	active = true
	collision.disabled = false


func deactivate():
	hide()
	active = false
	collision.disabled = true


func handle_active():
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_RIGHT) and _targeted and active:
		catch_and_remove()


func catch_and_remove():
	deactivate()
	caught.emit()
	TransitionActions.to_next_scene.emit()
